//ver01の改訂版

var http = require('http');
var url = require('url');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var ejs = require('ejs');

var server = http.createServer(listener);
server.listen(8080);
console.log('Server start');

function listener(req, res) {
    var urlInfo = url.parse(req.url, true);
    var pathname = urlInfo.pathname;
    
    if (pathname == '/') {
	//最初の画面
	fs.readFile('./in.html','utf-8',function(err,data){
	    res.writeHeader(200,{'Content-type':'text/html'});
	    res.write(data);
	    res.end();
	});
    }
    else if (pathname == '/post') {
	//移動後画面
	if (req.method.toLowerCase() == 'post') {
	    var form = new formidable.IncomingForm();
	    form.parse(req, function(err, fields, files) {
		var tokui = fields.tokui;
		if (!tokui) {
		    tokui = '\n';
		}

		//文字列の分割
		if(tokui.indexOf(',')){
		    var kekka = tokui.split(",");
		}else if(tokui.indexOf(' ')){//これは分割できてない
		    var kekka = tokui.split(" ");
		}else{
		    var kekka = tokui;
		}
		if(kekka.length > 3){
		    tokui = 'many'
		}else{
		    //表示のための工夫
		    for(i=1; i<kekka.length; i++){
			kekka[i] = 'と'+ kekka[i];
		    }
		}
		
		if(tokui == "\n"){
		    //何も入力されていない時
		    fs.readFile('./err.html','utf-8',function(err,data){
			res.writeHeader(200,{'Content-type':'text/html'});
			res.write(data);
			res.end();
		    });
		}
		else if(tokui == 'many'){
		    //4つ以上入力があるとき
		    fs.readFile('./many.html','utf-8',function(err,data){
			res.writeHeader(200,{'Content-type':'text/html'});
			res.write(data);
			res.end();
		    });
		}
		else if(tokui == "ない" || tokui == "無い"){
		    //得意なものが「ない」とき
		    var rand = Math.random();
		    console.log("rand = "+rand);
		    if (rand<0.6) {
			var no = fs.readFileSync('./no01.ejs','utf-8');
		    } else if (rand<0.8) {
			var no = fs.readFileSync('./no02.ejs','utf-8');
		    } else {
			var no = fs.readFileSync('./no03.ejs','utf-8');
		    }
		    var data = ejs.render(no,{tokui: tokui});
		    res.writeHeader(200,{'Content-type':'text/html'});
		    res.write(data);
		    res.end();   
		}
		else if (tokui == "かみひこうき" || tokui == "紙飛行機") {
		    var plane = fs.readFileSync('./plane.ejs','utf-8');
		    var data = ejs.render(plane,{tokui: tokui});
		    res.writeHeader(200,{'Content-type':'text/html'});
		    res.write(data);
		    res.end();
		}
		else if (tokui == "かりごっこ" || tokui == "狩りごっこ") {
		    var hunt = fs.readFileSync('./hunt.ejs','utf-8');
		    var data = ejs.render(hunt,{tokui: tokui});
		    res.writeHeader(200,{'Content-type':'text/html'});
		    res.write(data);
		    res.end();
		}
		else {
		    //得意なものが１〜３個のとき
		    var nomal =  fs.readFileSync('./nomal.ejs','utf-8');
		    var data = ejs.render(nomal,
					  {tokui1: kekka[0],
					   tokui2: kekka[1],
					   tokui3: kekka[2]
					  });
		    res.writeHeader(200,{'Content-type':'text/html'});
		    res.write(data);
		    res.end();		   
		}      
	    });
	}
	else{
	    res.statusCode = 400;
	    res.setHeader('Content-type', 'text/utf-8');
	    res.write('あれれ〜？\n');
	    res.end();
	}
    }
    else{
	res.statusCode = 404;
	res.setHeader('Content-type', 'text/plain');
	res.write('Not found');
	res.end();
    }
}

function escape(str) {
    str = str.replace(/&(?!\w+;)/g, '&')
    str = str.replace(/</g, '<')
    str = str.replace(/>/g, '>')
    str = str.replace(/"/g, '"');
    return str;
}
